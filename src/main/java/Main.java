import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;

@Mod(name = "My Mod", modid = "MyMod", version = "1.0")
public class Main {

    // This method runs when Minecraft starts your mod.
    @EventHandler
    public void start(FMLInitializationEvent event) {
        // Register other mod classes in here.
    }
}
