Minecraft Java Modding: Starter Project
========================================

## Outline

You can use this project to quickly start your own Minecraft mod using Java and Minecraft Forge.

To get started, first follow the _Installation_ instructions. Then follow the _Mod Setup_ instructions to start coding, building, and running your mod!

## Installation

Run all of these one-time installation steps in order.

#### 1. [Java Development Kit (JDK) 1.8][1]

* Download the appropriate installer (Windows, Mac, 32-bit, 64-bit, etc.).
* Run the installer using only the default settings.
* Make a note of where the JDK was installed.

#### 2. [Minecraft 1.10.2][3]

* Download the appropriate installer (Windows or Mac).
* Run the installer using the default settings.
* Launch Minecraft, login, and use a profile that uses Minecraft 1.10.2.
* Click Play to make sure the appropriate assets are installed.
* Exit Minecraft.

#### 3. [IntelliJ IDEA Community Edition 2016.2][2]

* Download the Community Edition of IntelliJ IDEA for your system.
* Run the installer.
  1. Check `64-bit` when asked to add a Desktop shortcut.
  2. Click `Skip and Set Defaults` when given the option.
* Launch IntelliJ IDEA.
* Configure the JDK:
  1. Select `Configure > Project Defaults > Project Structure`.
  2. Select `New... > JDK` below "Project SDK".
  3. Choose the directory where JDK 1.8 was installed and click `OK`.
  4. Select "1.8" in the list below "Project SDK".
  5. Click `OK`.

## Mod Setup

Run all of these steps in order before each lab.

#### 1. Download Files

* Go to [Minecraft Modding: Starter Project][6] on GitLab (you must be logged in).
* Select `Download ZIP` from the [![Download][7]][6] menu.
* Unzip the file to a location such as the Desktop.
* Rename the directory to something easy to read. _Example: `minecraft-java-starter`_

#### 2. Initialize the Project

Open the project:

* Run IntelliJ IDEA.
* Click `Import Project`.
* Find the unzipped project directory and select `build.gradle` in the appropriate session's sub-directory. _Example: `Desktop/minecraft-java-starter/build.gradle`_
* Make sure the following options are set:
  * Check `Use auto-import`.
  * Select `1.8` as the Gradle JVM.
  * Enter `-Xmx2G -Dorg.gradle.jvmargs=-Xmx3G` in the Gradle VM options box.
* Click `OK`.

Configure the workspace:

* Uncheck `Show Tips on Startup` and close the "Tip of the Day" window.
* Select `View > Tool Windows > Gradle`.
* Navigate to `Tasks > forgegradle` in the panel that appears.
* Double-click `setupDecompWorkspace`. This step may take several minutes, but should show [![Run build][8]][8] in the panel at the bottom of the screen when it is done.
* Double-click `genIntellijRuns`. Click `Yes` when asked to reload the project.

Build the mod:

* Navigate to `Tasks > build` in the Gradle projects panel.
* Double-click `build` and wait for "BUILD SUCCESSFUL" to display in the bottom panel.

Run the mod:

* Navigate to `Tasks > forgegradle` in the Gradle projects panel.
* Double-click `runClient` and wait for Minecraft to start.
* Click the `Mods` button and find the mod in the list.
* Quit Minecraft.



[1]: http://www.oracle.com/technetwork/java/javase/downloads/index.html "Java SE Downloads"
[2]: https://www.jetbrains.com/idea/download/index.html "IntelliJ IDEA Downloads"
[3]: https://minecraft.net/en/ "Minecraft Download"
[4]: http://files.minecraftforge.net/ "Minecraft Forge Downloads"
[5]: https://www.youtube.com/watch?v=0F7Bhswtd_w "Minecraft Forge Installation Instructions (Video)"
[6]: https://gitlab.com/tinkrlab/minecraft-java-skeleton "Minecraft Java Modding: Starter Project on GitLab"
[7]: https://gitlab.com/tinkrlab/media/raw/master/img/download.jpg "Download Icon"
[8]: https://gitlab.com/tinkrlab/media/raw/master/img/run-build-success.jpg "Run Build Success Icon"
